#ifndef MAINTABLE_H
#define MAINTABLE_H

#include <QMainWindow>
#include "appcontext.h"
#include <QMessageBox>
#include "entrypoint.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainTable; }
QT_END_NAMESPACE

void notify(AppContext * context);

class MainTable : public QMainWindow
{
    Q_OBJECT

public:
    MainTable(QWidget *parent = nullptr);
    ~MainTable();

private slots:
    void on_openFile_clicked();
    void on_evalMetric_clicked();
    void on_loadData_clicked();
    void clearMetrics();

private:
    AppContext context;
    Ui::MainTable *ui;

    void reopenTable();
    void clearTable();
    int sendDataFromTable(enum Operation);
    void showData(Linkedlist list);
};
#endif // MAINTABLE_H
