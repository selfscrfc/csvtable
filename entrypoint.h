#ifndef ENTRYPOINT_H
#define ENTRYPOINT_H

#include "appcontext.h"

enum Operation {
    loadData,
    getData,
    clearData,
    evaluate
};

bool doOperation(Operation operation, AppContext* context);

#endif // ENTRYPOINT_H
