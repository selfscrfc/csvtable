#include "entrypoint.h"
#include "appcontext.h"
#include "logic.h"

bool doOperation(Operation operation, AppContext* context) {
    bool result = 1;
    switch (operation) {
    case loadData:
        loadDataFromTable(context);
        break;
    case getData:
        getFileData(context);
        break;
    case clearData:
        clear(context);
        break;
    case evaluate:
        calculate(context);
        break;
    default:
        result = 0;
    }
    return result;
};
