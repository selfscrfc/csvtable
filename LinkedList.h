#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iostream>
#include <list>

class Node {

public:
    std::list<std::string> data;
    Node* next;
    Node(std::list<std::string> data);
};

class Linkedlist {

public:
    int listLen;
    Node* head;

    Linkedlist();
    void insertNode(std::list<std::string>);
    void deleteNode(int);
    void clear();
};

#endif // LINKEDLIST_H
