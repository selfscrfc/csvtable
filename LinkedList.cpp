#include "LinkedList.h"
#include <iostream>
using namespace std;

Node::Node(std::list<std::string> data){
    this->data = data;
    next = NULL;
}

Linkedlist::Linkedlist(){
    head = NULL;
    listLen = 0;
}

void Linkedlist::deleteNode(int nodeInd){
    Node *temp1 = head, *temp2 = NULL;

    if (nodeInd <= 0) {
        head = head->next;
        delete temp1;
        return;
    }

    while (nodeInd-- > 0) {

        temp2 = temp1;

        temp1 = temp1->next;
    }

    temp2->next = temp1->next;

    delete temp1;
    listLen -= 1;
}

void Linkedlist::insertNode(std::list<std::string> data){
    Node* newNode = new Node(data);

    if (head == NULL) {
        head = newNode;
        listLen += 1;
    } else {
        Node* temp = head;
        while (temp->next != NULL) {

            temp = temp->next;
        }

        temp->next = newNode;
        listLen += 1;
    }
}

void Linkedlist::clear(){

    Node *tmp1 = head, *tmp2 = NULL;

    while (listLen-- > 0) {
        tmp2 = tmp1;
        tmp1 = tmp1->next;
        tmp2->data.clear();
        tmp2->next = NULL;
    }
    listLen = 0;
    head = NULL;
}

