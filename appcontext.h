#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include <list>
#include "LinkedList.h"

#define BUFF_SIZE 100
#define LINE_SIZE 256

struct AppContext {
    FILE* fs = NULL;
    Linkedlist list;
    Linkedlist answerList;
    std::list<std::string> headers;
    std::list<std::string> digitHeaders;
    std::list<double> medianArray;
    std::string filename;
    std::string error;
    std::string region;
    std::string colKey;
    double metrics[3];
    int regionInd           = 0;
    int colInd              = -1;
    int cols                = -1;
};

//void contextInit(AppContext* context);

#endif // APPCONTEXT_H
