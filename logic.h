#ifndef LOGIC_H
#define LOGIC_H

#include "appcontext.h"

#define SYMBOL_NOT_FOUND -1

std::list<std::string> strSplit(std::string line, int* cols, char delim = ',');
void getFileData(AppContext* context);
bool is_number(std::string s);
void loadDataFromTable(AppContext* context);
void calculate(AppContext* context);
int findColInd(std::list<std::string> list, std::string colKey);
bool haveRegion(std::list<std::string> list, AppContext* context);
void clear(AppContext* context);
void evaluateMedian(AppContext* context);


#endif // LOGIC_H
