#include "maintable.h"
#include "ui_maintable.h"
#include "entrypoint.h"
#include <QFileDialog>
#include <QDebug>
MainTable::MainTable(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainTable)
{

    ui->setupUi(this);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    MainTable::connect(ui->chooseColBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                       this, &MainTable::clearMetrics);
}
MainTable::~MainTable()
{
    delete ui;
}

QStringList ConvertRowToQTFormat(std::list<std::string> row, int size)
{
    QStringList qsl = {};

    for(int i = 0; i < size; i++)
    {
        qsl.append(QString::fromStdString(row.front()));
        row.pop_front();
    }

    return qsl;
}

void MainTable::showData(Linkedlist list) {
    if (ui->tableWidget->columnCount() == 0) {
        ui->tableWidget->setColumnCount(context.cols);
        QStringList QColumns = ConvertRowToQTFormat(context.headers, context.cols);
        ui->tableWidget->setHorizontalHeaderLabels(QColumns);
        ui->tableWidget->setRowCount(0);
    }
    Node* currentNode = list.head;
    if (list.listLen != 0)
    {
        for (int i = 0; i < list.listLen; i++)
        {
            QStringList currentRow = ConvertRowToQTFormat(currentNode->data, context.cols);
            currentNode = currentNode->next;
            ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);
            for (int j = 0; j < currentRow.count(); j++)
            {
                QTableWidgetItem *item = new QTableWidgetItem();
                item->setText(currentRow.at(j));
                ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, j, item);
            }
        }
    }
}

void MainTable::on_openFile_clicked() {
    clearTable();
    ui->chooseColBox->clear();
    QString filename = QFileDialog::getOpenFileName(this, "Open CSV File", "/home/selfscrfc/Projects-QT/CSVTable/CSV_FILES/", "CSV File (*.csv)");
    ui->fileLabel->setText(filename);
    context.filename = filename.toStdString();
    reopenTable();
}

void MainTable::on_evalMetric_clicked(){
    clearMetrics();
    int result = 1;
    context.metrics[0] = 0;
    context.metrics[1] = 0;
    context.metrics[2] = 0;
    context.colInd = -1;
    context.medianArray.clear();
    if (ui->fileLabel->text() == "") {
        context.error = "Нет открытого файла!";
    } else if (context.region == "") {
        context.error = "Регион не выбран!";
    } else if (ui->chooseColBox->count() == 0){
        context.error = "Либо файл поврежден, либо нет полей, по которым можно произвести расчет!";
    } else {
        context.colKey = ui->chooseColBox->currentText().toStdString();
        result = sendDataFromTable(evaluate);
        std::string ans = "Min       : " + std::to_string(context.metrics[0]) + "\n"
                + "Median : " + std::to_string(context.metrics[1]) + "\n"
                + "Max       : " + std::to_string(context.metrics[2]);
        ui->resultLabel->setText(QString::fromStdString(ans));
    }
    if (!result) {
        context.error = "Send data form table error!";
    }
    if (context.error != "")
        ui->resultLabel->setText("");
    notify(&context);
}

void MainTable::on_loadData_clicked() {
    reopenTable();
    int result = 1;
    ui->resultLabel->setText("");
    if (ui->chooseRegBox->text() == "") {
    } else if (ui->fileLabel->text() == "") {
        context.error = "Нет открытого файла!";
    } else if (ui->chooseRegBox->text() == "") {
        context.error = "Регион не выбран!";
    } else if (ui->chooseRegBox->text() == QString::fromStdString(context.region)){
        context.error = "Данные по этому региону уже загружены!";
    } else {
        context.region = ui->chooseRegBox->text().toStdString();
        result = sendDataFromTable(loadData);
        if (context.answerList.head == NULL) {
            context.error = "Этого региона нет в списке!";
        } else {
            ui->tableWidget->setRowCount(0);
            showData(context.answerList);
        }
    }
    if (!result) {
        context.error = "Send data form table error!";
    }
    if (context.error != "")
        clearTable();
    notify(&context);
    context.answerList.clear();
}

void MainTable::reopenTable() {
    clearTable();
    ui->chooseColBox->clear();
    if (context.filename != "") {
        ui->tableWidget->setRowCount(0);
        int result = 0;
        do {
            result = doOperation(getData, &context);
            showData(context.list);
        } while (context.list.listLen == BUFF_SIZE && result);

        QStringList QColumns = ConvertRowToQTFormat(context.digitHeaders, context.digitHeaders.size());
        for (size_t i = 0; i < context.digitHeaders.size(); i++) {
            ui->chooseColBox->addItem(QColumns[i]);
        }
    } else {
        context.error = "Выберите корректный файл!";
    }
    if (context.error != "")
        clearTable();
    notify(&context);
}

void notify(AppContext* context) {
    if (context->error != "") {
        QMessageBox msgbox;
        msgbox.setWindowTitle("Error!");
        msgbox.setText(QString::fromStdString(context->error));
        msgbox.exec();
        context->error = "";
    }
}

int MainTable::sendDataFromTable(enum Operation oper) {
    int counter = 0;
    context.list.clear();
    int result = 1;
    for (int i = 0; i < ui->tableWidget->rowCount() && result; i++)
    {
        counter++;
        std::list<std::string> list;
        for (int j = 0; j < ui->tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *item = ui->tableWidget->item(i,j);
            std::string str = item->text().toStdString();
            list.push_back(str);
        }
        context.list.insertNode(list);
        if (counter == BUFF_SIZE) {
            result = doOperation(oper, &context);
            context.list.clear();
            counter = 0;
        }
    }
    if (counter != 0 && result) {
        result = doOperation(oper, &context);
    }
    return result;
}

void MainTable::clearMetrics() {
    ui->resultLabel->setText("");
}

void MainTable::clearTable() {
    int result = doOperation(clearData, &context);
    ui->resultLabel->setText("");
    ui->tableWidget->setColumnCount(0);
    ui->tableWidget->setRowCount(0);
    if (!result) {
        context.error = "Error clearing table";
        notify(&context);
    }
}



