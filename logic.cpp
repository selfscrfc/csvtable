#include "logic.h"
#include <string>
#include <iostream>
#include <QDebug>


void extraFunc(AppContext *context, char* line, FILE* fs);
void getFileData(AppContext* context) {
    if (context->fs == NULL) {
        context->fs = fopen(context->filename.c_str(), "r");
    }
    FILE* fs = context->fs;
    char line[LINE_SIZE];
    context->list.clear();
    bool flag = 1;
    if (context->headers.empty()) {
        fgets(line, LINE_SIZE-1, fs);
        context->headers = strSplit(line, &context->cols);
        if (haveRegion(context->headers, context) == 0) {
            context->headers.clear();
            context->error = "No region column!";
            flag = 0;
        }
    }
    if (flag) {
        extraFunc(context, line, fs);
    }
}

void extraFunc(AppContext *context, char* line, FILE* fs) {
    while (context->list.listLen < BUFF_SIZE && fgets(line, LINE_SIZE-1, fs) != NULL) {
        context->list.insertNode(strSplit(line, &context->cols));
    }
    if (context->digitHeaders.empty()) {
        std::list<std::string> headList = context->list.head->data;
        std::list<std::string> headListNames = context->headers;
        for (int i = 0; i < context->cols; i++) {
            if (is_number(headList.front())) {
                context->digitHeaders.push_back(headListNames.front());
            }
            headList.pop_front();
            headListNames.pop_front();
        }
    }
    if (context->list.listLen != BUFF_SIZE) {
        fclose(context->fs);
        context->fs = NULL;
    }
}

void clear(AppContext* context) {
    context->list.clear();
    context->answerList.clear();
    context->headers.clear();
    context->digitHeaders.clear();
    context->region         = "";
    context->metrics[0]     = 0;
    context->metrics[1]     = 0;
    context->metrics[2]     = 0;
    context->cols           = -1;
    context->colKey         = "";
}

void calculate(AppContext* context) {
    Node * node = context->list.head;
    int counter = context->list.listLen;
    bool initMetrics = false;
    double data;
    if (context->colInd == -1) {
        context->colInd = findColInd(context->headers, context->colKey);
        initMetrics = true;
    }
    while (counter-- > 0) {
        for (int i = 0; i < context->colInd; i++)
            node->data.pop_front();
        if (node->data.front() != "" && !(is_number(node->data.front()) == 0)) {
            if (node->data.front().find(".") != SYMBOL_NOT_FOUND) {
                std::string str = node->data.front().replace(node->data.front().find("."), 1, ",");
                node->data.pop_front();
                node->data.push_front(str);
            }

            data = std::stod(node->data.front());
            if (initMetrics) {
                context->metrics[0] = data;
                context->metrics[1] = data;
                context->metrics[2] = data;
                initMetrics = false;
            }
            context->metrics[0] = context->metrics[0] > data ? data : context->metrics[0];
            context->metrics[2] = context->metrics[2] < data ? data : context->metrics[2];
            context->medianArray.push_back(data);
        }
        node = node->next;
    }
    evaluateMedian(context);
}

void evaluateMedian(AppContext* context) {
    if (context->list.listLen != BUFF_SIZE) {
        context->medianArray.sort();

        int size = context->medianArray.size();
        for (int i = 0; i < (int)size/2 - (size+1)%2; i++)
            context->medianArray.pop_front();
        context->metrics[1] = context->medianArray.front();
        if (size%2 == 0 && context->medianArray.size() != 0) {
            context->medianArray.pop_front();
            context->metrics[1] = (context->metrics[1] + context->medianArray.front())/2;
        }
    }
}

void loadDataFromTable(AppContext* context) {
    Node * node = context->list.head;
    int counter = context->list.listLen;
    while (counter-- > 0) {
        std::list<std::string> list = node->data;
        for (int i = 0; i < context->regionInd; i++)
            list.pop_front();
        std::string nodeReg = list.front();
        if (nodeReg == context->region) {
            context->answerList.insertNode(node->data);
        }
        node = node->next;
    }
}

bool is_number(std::string s){
    bool result = 1;
    for (size_t i = 0; i < s.length(); i++) {
        if (!std::isdigit(s[i]) && s[i] != '.' && s[i] != '-') {
            result = 0;
            break;
        }
    }
    if (s == " ")
        result = 0;
    return result;
}

std::list<std::string> strSplit(std::string line, int* cols, char delim){
    int counter = 0;

    if (*cols == -1) {
        for (size_t i = 0; i < line.length(); i++) {
            if (line[i] == delim) {
                counter += 1;
            }
        }
        counter++;
        *cols = counter;
    } else {
        counter = *cols;
    }

    std::list<std::string> result;

    size_t ind = line.find(delim);

    for (int i = 0; i < counter ; i++) {
        result.push_back(line.substr(0, ind));
        line = line.substr(ind + 1, line.length());
        ind = line.find(delim);
    }
    if (result.back().back() == '\n')
        result.back().pop_back();

    return result;
}

bool haveRegion(std::list<std::string> list, AppContext* context) {
    int result = 0, size = list.size(), saveSize = list.size();
    while (size-- > 0){
        std::string colName = list.front();
        if (colName == "region") {
            result = 1;
            break;
        }
        colName.pop_back(); // '\n' check
        if (colName == "region") {
            result = 1;
            break;
        }
        list.pop_front();
    }
    if (result) {
        context->regionInd = saveSize - size - 1;
    }
    return result;
}

int findColInd(std::list<std::string> list, std::string colKey) {
    int colInd = 0;
    int length = list.size();
    for (int i = 0; i < length; i ++) {
        if (list.front() == colKey) {
            colInd = i;
            break;
        }
        list.pop_front();
    }

    return colInd;
}




